const base_url = "https://readerapi.codepolitan.com/";

async function getArticles(){
  if ('caches' in window) {
    caches.match(`${base_url}articles`)
     .then(function(response) {
      if (response) {
        response.json().then(function (data) {
          let articlesHTML = "";
          data.result.forEach(function(article) {
            articlesHTML += `
              <div class="card">
                <a href="./article.html?id=${article.id}">
                    <div class="card-image waves-effect waves-block waves-light">
                      <img src="${article.thumbnail}" />
                    </div>
                </a>
                <div class="card-content">
                    <span class="card-title truncate">${article.title}</span>
                    <p>${article.description}</p>
                </div>
              </div>
            `;
          });
          document.getElementById("articles").innerHTML = articlesHTML;
        })
      }
    })
  }
    
    await fetch(`${base_url}articles`)
     .then((response) => {
        if(response.status == 200){
            return Promise.resolve(response)
        }else{
            console.log(`Error: ${response.status}`)
            return Promise.reject(new Error(response.statusText))
         }
     })
     .then((res) => {
        return res.json()
     })
     .then( async (data) => {
        let articlesHtml = ""

        await data.result.forEach((articles) => {
            articlesHtml += `
            <div class="card">
                <a href="./article.html?id=${articles.id}">
                  <div class="card-image waves-effect waves-block waves-light">
                    <img src="${articles.thumbnail}" />
                  </div>
                </a>
                <div class="card-content">
                  <span class="card-title truncate">${articles.title}</span>
                  <p>${articles.description}</p>
                </div>
            </div>
            `
         })

         document.getElementById("articles").innerHTML = articlesHtml
     })
}
getArticles()