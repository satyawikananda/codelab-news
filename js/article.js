const base_url = "https://readerapi.codepolitan.com/";

async function getArticleById(){
    // To get the params url
    const urlParams = new URLSearchParams(window.location.search)
    const idParam = urlParams.get("id")
  
    await fetch(`${base_url}article/${idParam}`)
     .then((response) => {
       if(response.status == 200){
         return Promise.resolve(response)
       }else{
         console.log(`Error: ${response.status}`)
         return Promise.reject(new Error(response.statusText))
       }
     })
     .then((res) => {
       return res.json()
     })
     .then((data) => {
       let article = `
       <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <img src="${data.result.cover}" />
            </div>
            <div class="card-content">
              <span class="card-title">${data.result.post_title}</span>
              ${snarkdown(data.result.post_content)}
            </div>
        </div>
       `

       document.getElementById("body-content").innerHTML = article
     })
  }
  
getArticleById()