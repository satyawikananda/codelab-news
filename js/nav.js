document.addEventListener("DOMContentLoaded", function(){
    const elems = document.querySelectorAll('.sidenav')
    M.Sidenav.init(elems)
    let page = window.location.hash.substr(1)
    if(page == "") page = "home"
    loadPage(page)
    loadnav()

    function loadnav(){
        const xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function(){
            if(this.readyState == 4){
                if(this.status != 200) return

                document.querySelectorAll('.sidenav, .topnav').forEach((elm) => {
                    elm.innerHTML = xhttp.responseText
                })

                document.querySelectorAll('.sidenav a, .topnav a').forEach((elm) => {
                    elm.addEventListener('click', (e) => {
                        const sidenav = document.querySelector('.sidenav')
                        M.Sidenav.getInstance(sidenav).close()

                        page = e.target.getAttribute('href').substr(1)
                        loadPage(page)
                    })
                })
            }
        }
        xhttp.open("GET", "nav.html", true)
        xhttp.send()
    }
    
    function loadPage(page){
        const xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function(){
            if(this.readyState == 4){
                let content = document.querySelector('#body-content')
                if(this.status == 200){
                    content.innerHTML = xhttp.responseText
                }else if(this.status == 404){
                    content.innerHTML = `<p> 404 not found </p>`
                }else{
                    content.innerHTML = `<p> Something went wrong</p>`
                }
            }
        }
        xhttp.open("GET", `pages/${page}.html`, true)
        xhttp.send()
    }
})