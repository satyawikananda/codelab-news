if ("serviceWorker" in navigator){
    window.addEventListener("load", () => {
        navigator.serviceWorker
         .register('../service-worker.js')
         .then(() => {
            console.log("Service worker berhasil di daftar")
         })
         .catch((error) => {
             console.log("Service worker gagal didaftar")
         })
    })
}else{
    console.log("ServiceWorker belum didukung browser ini");
}