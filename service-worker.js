const CACHE_NAME = "myfirstpwa_v5"
const urlToCache = [
    "/",
    "/nav.html",
    "/index.html",
    "/pages/about.html",
    "/pages/contact.html",
    "/pages/home.html",
    "/css/materialize.min.css",
    "/js/materialize.min.js",
    "/js/nav.js",
    "/js/home.js",
    "/js/article.js",
    "/js/script.js"
]

self.addEventListener("install", (e) => {
    e.waitUntil(
        caches.open(CACHE_NAME)
            .then((cache) => {
                return cache.addAll(urlToCache)
            })
    )
})

self.addEventListener("fetch", (event) => {
    const base_url = "https://readerapi.codepolitan.com/";

    if(event.request.url.indexOf(base_url) > -1){
        event.respondWith(
            caches.open(CACHE_NAME)
             .then((cache) => {
                 return fetch(event.request)
                  .then((response) => {
                      cache.put(event.request.url, response.clone())
                      return response
                  })
             })
        )
    }else{
        event.respondWith(
            caches.match(event.request, { ignoreSearch: true })
             .then((response) => {
                return response || fetch(event.request)
             })
        )
    }
})

self.addEventListener("activate", (e) => {
    e.waitUntil(
        caches.keys()
         .then((cacheNames) => {
             return Promise.all(
                 cacheNames.map((cacheName) => {
                     if(cacheName != CACHE_NAME){
                         console.log(`Serviceworker: cache ${cacheName} has been deleted`)
                         return caches.delete(cacheName)
                     }
                 })
             )
         })
    )
})